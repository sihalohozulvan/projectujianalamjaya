package com.gudangtest.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    TextView txtHello;
    private String bilangan;
    private String KEY_BILANGAN = "Output";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        txtHello = (TextView) findViewById(R.id.txtHello);
        Bundle extras = getIntent().getExtras();
        bilangan = extras.getString(KEY_BILANGAN);
//        int temp = bilangan;
        int number = Integer.parseInt(bilangan);
        String data = "";
        for(int i=1; i <= number; i ++){
            if(i % 5 == 0 || i%5 == 2){
                data += "*";
            }
            else{
                data += String.valueOf(i);
            }
            if(i != number){
                data += ",";
            }

        }
        txtHello.setText("Bilangan, " + data);
    }
}