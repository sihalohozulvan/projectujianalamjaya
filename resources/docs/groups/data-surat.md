# Data Surat

APIs untuk data surat

## Get count all surat


Untuk mengambil jumlah surat keluar dan surat masuk di lingkungan pemerintah kota Medan

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/jumlah-surat" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/jumlah-surat"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETapi-jumlah-surat" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-jumlah-surat"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-jumlah-surat"></code></pre>
</div>
<div id="execution-error-GETapi-jumlah-surat" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-jumlah-surat"></code></pre>
</div>
<form id="form-GETapi-jumlah-surat" data-method="GET" data-path="api/jumlah-surat" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-jumlah-surat', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-jumlah-surat" onclick="tryItOut('GETapi-jumlah-surat');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-jumlah-surat" onclick="cancelTryOut('GETapi-jumlah-surat');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-jumlah-surat" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/jumlah-surat</code></b>
</p>
</form>


## Get count surat Per OPD


Untuk mengambil jumlah surat keluar dan surat masuk
Per OPD di lingkungan pemerintah kota Medan

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/jumlah-surat-peropd" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/jumlah-surat-peropd"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETapi-jumlah-surat-peropd" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-jumlah-surat-peropd"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-jumlah-surat-peropd"></code></pre>
</div>
<div id="execution-error-GETapi-jumlah-surat-peropd" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-jumlah-surat-peropd"></code></pre>
</div>
<form id="form-GETapi-jumlah-surat-peropd" data-method="GET" data-path="api/jumlah-surat-peropd" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-jumlah-surat-peropd', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-jumlah-surat-peropd" onclick="tryItOut('GETapi-jumlah-surat-peropd');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-jumlah-surat-peropd" onclick="cancelTryOut('GETapi-jumlah-surat-peropd');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-jumlah-surat-peropd" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/jumlah-surat-peropd</code></b>
</p>
</form>



