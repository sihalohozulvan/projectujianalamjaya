---
title: Laravel Documentation

language_tabs:
- bash
- javascript

includes:
- "./prepend.md"
- "./authentication.md"
- "./groups/*"
- "./errors.md"
- "./append.md"

logo: 

toc_footers:
- <a href="./collection.json">View Postman collection</a>
- <a href="./openapi.yaml">View OpenAPI (Swagger) spec</a>
- <a href='http://github.com/knuckleswtf/scribe'>Documentation powered by Scribe ✍</a>

---

# Introduction



Documentasi API Sarana untuk pengembangan aplikasi sarana berbasis web dan android..

<aside>Silahkan scroll kebawah untuk melihat REST-API secara keseluruhan.</aside>

<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>
<script>
    var baseUrl = "http://localhost";
</script>
<script src="js/tryitout-2.7.10.js"></script>

> Base URL

```yaml
http://localhost
```