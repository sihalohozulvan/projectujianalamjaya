<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Surat extends Model
{
    //
    protected $table = 'tbl_surat';

    public function getSurat(){
        return $this->hasMany('App\Surat', 'id_pegawai');
    }

    public function getSuratByProses(){
        $surat = Surat::where('acc', '=', '2')->get();
        return $surat;
    }

    public static function getCountSuratKeluarByStatus($status){
        $surat_keluar = Surat::where([
            'acc' => $status,
            'type_surat' => 'keluar'])->count();
        return $surat_keluar;
    }

    public static function getCountSuratMasuk(){
        $surat_masuk = Surat::where([
            'type_surat' => 'masuk'])->count();
        return $surat_masuk;
    }
}
