<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArsipSuratMasuk extends Model
{
    //
    protected $table = 'tbl_surat_arsip_masuk';

    public static function getArsipSuratMasukByOPD($opd){
        $arsip_surat_masuk = ArsipSuratMasuk::where(['kode_unor' => $opd])->count();
        return $arsip_surat_masuk;
    }
}
