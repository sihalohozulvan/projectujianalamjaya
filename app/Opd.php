<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opd extends Model
{
    //
    protected $table = 'sys_unor';
    public static function getAllOpd(){
        $opd = Opd::withCount(['arsip_surat_masuk','arsip_surat_keluar'])
        ->where('kode_unor_opd','<>', null)
        // ->where('arsip_surat_masuk_count','>', 0)
        ->groupBy('kode_unor_opd')->get();
        return $opd;
    }

    public function arsip_surat_masuk()
    {
        return $this->hasMany('App\ArsipSuratMasuk', 'kode_unor', 'kode_unor_opd');
    }

    public function arsip_surat_keluar()
    {
        return $this->hasMany('App\ArsipSuratKeluar', 'kode_unor', 'kode_unor_opd');
    }
}
