<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TestApiController extends Controller
{
    //

    public function input_user(Request $request){
        $input_user = $request->all();
        $string = [];
        if(!isset($input_user['bilangan'])){
            $bilang_inputan_user = 15;
        }else{
            if($input_user == ''){
                $bilang_inputan_user = 15;
            }else{
                $bilang_inputan_user = $input_user['bilangan'];
            }
        }
        

        for($i=1;$i<=$bilang_inputan_user; $i++){
            if($i % 5 == 0 && $i % 3 == 0){
                $string[] = "tiga lima";
            }elseif($i % 5 == 0){
                $string[] = "lima";
            }elseif($i % 3 == 0){
                $string[] = "tiga";
            }else{
                $string[] = $i;
            }
        }
        return response()->json($string);
    }
}
