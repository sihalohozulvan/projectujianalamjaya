<?php
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Tabeluser;
use App\Tableuser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\User;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Illuminate\Support\Facades\Redis;
use Twilio\Http\Client;
use Validator;



/**
 * @group Authenticating
 *
 * APIs untuk Auth
 */
class AuthController extends Controller
{
    

    // public function register(Request $request)
    // {
    //     $user = Tabeluser::create([
    //         'username' => $request->name,
    //         'passwd' => Hash::make($request->passwd),
    //         'change_password' => 0,
    //         'level' => 'admin'
    //      ]);

    //     $token = $user->createToken('auth_token')->plainTextToken;

    //     return response()
    //         ->json(['data' => $user,'access_token' => $token, 'token_type' => 'Bearer', ]);
    // }

     /**
	 * Login
     * 
     * Untuk melakukan login user dengan menggunakan username (NIP) dan password
     * 
     * @bodyParam username string required NIP
     * @bodyParam password string required Password
     * 
     * @response {
     * "status": "success",
     *      "code": 200,
     * "message": "Berhasil Login",
     * "data": {
     * "id": 16511,
     * "id_pegawai": null,
     * "username": "123456789",
     * "change_password": 0,
     * "status": 1,
     * "date_update": null,
     * "nik": null,
     * "token": null,
     * "status_login": null,
     * "level": null,
     * "updated_at": "2022-04-21T06:55:26.000000Z",
     * "created_at": "2022-04-21T06:55:26.000000Z"
     * },
     * "access_token": "16|ufz6V7mHuItD6EfTyRC5WWt0naMP3xoJk9Vft1tt",
     * "token_type": "Bearer"
     * }
	 */
    protected function login(Request $request)
    {

        $otp = rand(1000, 9999);
        $twilio_whatsapp_number = getenv("TWILIO_WHATSAPP_NUMBER");
        $account_sid = getenv("TWILIO_ACCOUNT_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");

        $client = new Client($account_sid, $auth_token);
        $message = "Your registration pin code is $otp";
        return $client->messages->create("whatsapp:085266479617", 
        array('from' => "whatsapp:$twilio_whatsapp_number", 'body' => $message));
    

        // $this->sendWhatsappNotification($otp, $user->phone_number);
        if(!auth()->attempt(['username' => $request->username, 'passwd' => $request->passwd])){
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }
        $user = Tabeluser::where(
            'username', $request['username']
            )->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;
        $response = [
            'status' => 'success',
            'code' => 200,
            'message' => 'Berhasil Login',
            'data' => $user,
            'access_token' => $token,
            'token_type' => 'Bearer',
        ];
        return response()->json($response);
    }

     /**
	 * Logout
     * 
     * Untuk melakukan logout user
     * 
     * @authenticated
	 */
    public function logout()
    {

        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });
        return [
            'message' => 'You have successfully logged out and the token was successfully deleted'
        ];
    }

    /**
	 * Get Profile User
	 */
    public function profile(){
        // Auth::user()->load('user');
        $data_user = auth()->user();
        return response()->json();
    }
    
}