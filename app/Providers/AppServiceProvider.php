<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Cache\RateLimiter;
use Illuminate\Http\Response;

// use Illuminate\Support\Facades\RateLimiter;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        
        // $limit = new RateLimiter($this->app['cache.store']);
        // $limit->clear('api');
        // $limit->for('api', function (Request $request) {
        //     return Limit::perMinute(3)->response(function () {
        //         return response('Too many request...', 429);
        //     });
        // });
    }
}
