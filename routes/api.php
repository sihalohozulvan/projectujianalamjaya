<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/





Route::post('/bilangan-positif', [App\Http\Controllers\API\TestApiController::class, 'input_user']);


// Route::post('/add-user', [App\Http\Controllers\API\AuthController::class, 'addUser']);
// //API route for register new user
// Route::post('/register', [App\Http\Controllers\API\AuthController::class, 'register']);
// //API route for login user
// Route::post('/login', [App\Http\Controllers\API\AuthController::class, 'login'])->name('login');
// //Protecting Routes
// Route::group(['middleware' => ['auth:sanctum']], function () {
//     //API profile
//     Route::get('/profile', [App\Http\Controllers\API\AuthController::class, 'profile']);

//     // API route for logout user
//     Route::post('/logout', [App\Http\Controllers\API\AuthController::class, 'logout']);
//     Route::get('/jumlah-surat', [App\Http\Controllers\API\GetSuratOPDController::class, 'getCountAllSurat']);
//     Route::get('/jumlah-surat-peropd', [App\Http\Controllers\API\GetSuratOPDController::class, 'getCountAllSuratPerOPD']);
// });


